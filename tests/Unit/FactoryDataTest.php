<?php

namespace HolaLuz\Tests\Unit;

use PHPUnit\Framework\TestCase;
use HolaLuz\Infrastructure\Handlers\Datahandlers\HandlerFactory;
use HolaLuz\Infrastructure\Handlers\Datahandlers\ArrayDataHandler;
use HolaLuz\Infrastructure\Handlers\Datahandlers\XmlDataHandler;
use HolaLuz\Infrastructure\Handlers\Datahandlers\JSONDataHandler;

class FactoryDataTest extends TestCase
{

    public function testCsvHandler()
    {
        $filepath = __DIR__ . '/../../etc/2016-readings.csv';
        $headers = true;
        $content = [];
        $handler = fopen($filepath, 'r');
        while (($row = fgetcsv($handler, 1024, ',')) !== false) {
            if ($headers) {
                $headers = false;
                continue;
            }
            $content[] = $row;
        }
        $handler = HandlerFactory::getInstance($content);
        $this->assertInstanceOf(ArrayDataHandler::class, $handler);
    }

    public function testJsonHandler()
    {
        $filepath = __DIR__ . '/../../etc/2016-readings.json';
        $handler = HandlerFactory::getInstance(file_get_contents($filepath));
        $this->assertInstanceOf(JSONDataHandler::class, $handler);
    }

    public function testXmlHandler()
    {

        $filepath = __DIR__ . '/../../etc/2016-readings.xml';
        $handler = HandlerFactory::getInstance(new \SimpleXMLElement(file_get_contents($filepath)));
        $this->assertInstanceOf(XmlDataHandler::class, $handler);
    }

    public function testErrorHandler()
    {
        $this->expectException(\Exception::class);
        $filepath = __DIR__ . '/../../etc/2016-readings.sql';
        HandlerFactory::getInstance($filepath);
    }

}
