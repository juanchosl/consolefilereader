<?php

namespace HolaLuz\Domain\Collections;

use HolaLuz\Domain\Entities\SuspiciousData;

class SuspiciousDataCollection extends AbstractDataCollection
{

    public function add(SuspiciousData $user): self
    {
        array_push($this->data, $user);
        return $this;
    }

    public function copy(): SuspiciousDataCollection
    {
        $col = new SuspiciousDataCollection();
        foreach ($this->data as $data) {
            $col->add($data);
        }
        return $col;
    }

}
