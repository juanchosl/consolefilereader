<?php

namespace HolaLuz\Tests\Integration;

use PHPUnit\Framework\TestCase;
use HolaLuz\Domain\ReadingService;
use HolaLuz\Infrastructure\Handlers\Filehandlers\HandlerFactory;
use HolaLuz\Domain\Entities\SuspiciousData;
use HolaLuz\Infrastructure\Handlers\Datahandlers\IDataReader;
use HolaLuz\Infrastructure\Handlers\Filehandlers\IFileReader;
use HolaLuz\Domain\Entities\UserData;
use HolaLuz\Domain\Collections\UserDataCollection;
use HolaLuz\Domain\Collections\SuspiciousDataCollection;
use HolaLuz\Infrastructure\Repositories\UserDataRepository;

class ServiceTest extends TestCase
{

    public function getService(IFileReader $filepath = null)
    {
        return new ReadingService(ReadingService::ALGORITHM_MEDIAN, 50);
    }

    public function testSuspiciousByFileCsv()
    {
        $this->getSuspiciousByFile(__DIR__ . '/../../etc/2016-readings.csv');
    }

    public function testSuspiciousByFileXml()
    {
        $this->getSuspiciousByFile(__DIR__ . '/../../etc/2016-readings.xml');
    }

//    public function testSuspiciousDirectly()
//    {
//        $filepath = __DIR__ . '/../../etc/2016-readings.csv';
//        $this->assertFileExists($filepath);
//        $service = $this->getService();
//        $suspicious = $service->getSuspiciousByOrigin(new DataAdapter(HandlerFactory::getInstance($filepath)));
//        $this->assertInstanceOf(SuspiciousDataCollection::class, $suspicious);
//        $this->assertContainsOnlyInstancesOf(SuspiciousData::class, $suspicious);
//    }

    protected function getSuspiciousByFile($filepath)
    {
        $this->assertFileExists($filepath);
        $service = $this->getService();
        $suspicious = $service->getSuspiciousByData((new UserDataRepository(HandlerFactory::getInstance($filepath)->getContent()))->getData());
        $this->assertInstanceOf(SuspiciousDataCollection::class, $suspicious);
        $this->assertContainsOnlyInstancesOf(SuspiciousData::class, $suspicious);
    }

    public function testSuspiciousByUserData()
    {
        $filepath = __DIR__ . '/../../etc/2016-readings.xml';
        $this->assertFileExists($filepath);
        $service = $this->getService();
        $suspicious = $service->getSuspiciousByData((new UserDataRepository(HandlerFactory::getInstance($filepath)->getContent()))->getData());
        $this->assertInstanceOf(SuspiciousDataCollection::class, $suspicious);
        $this->assertContainsOnlyInstancesOf(SuspiciousData::class, $suspicious);
    }

    public function testSuspiciousByUserDataCalculatedMaximun()
    {
        $value = 1000;
        $client = uniqid();
        $usersdata = new UserDataCollection();
        for ($i = 1; $i <= 12; $i++) {
            $usersdata->add(new UserData($client, "2022-{$i}", ($i == 1) ? $value * 3 : $value));
        }
        $service = $this->getService();
        $suspicious = $service->getSuspiciousByData($usersdata);
        $this->assertInstanceOf(SuspiciousDataCollection::class, $suspicious);
        $this->assertContainsOnlyInstancesOf(SuspiciousData::class, $suspicious);
        $suspicious->rewind();
        $this->assertEquals(1, $suspicious->current()->getMonth());
    }

    public function testSuspiciousByUserDataCalculatedMinimum()
    {
        $value = 1000;
        $client = uniqid();
        $usersdata = new UserDataCollection();
        for ($i = 1; $i <= 12; $i++) {
            $usersdata->add(new UserData($client, "2022-{$i}", ($i == 1) ? $value / 3 : $value));
        }
        $service = $this->getService();
        $suspicious = $service->getSuspiciousByData($usersdata);
        $this->assertInstanceOf(SuspiciousDataCollection::class, $suspicious);
        $this->assertContainsOnlyInstancesOf(SuspiciousData::class, $suspicious);
        $suspicious->rewind();
        $this->assertEquals(1, $suspicious->current()->getMonth());
    }

}
