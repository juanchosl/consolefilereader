<?php

namespace HolaLuz\Infrastructure\Handlers\Filehandlers;

use HolaLuz\Infrastructure\Handlers\Datahandlers\XmlDataHandler;
use HolaLuz\Infrastructure\Handlers\Datahandlers\IDataReader;

class XmlFileHandler implements IFileReader
{

    private $handler;

    public function __construct($filepath)
    {
        $this->handler = new \SimpleXMLElement(file_get_contents($filepath));
    }

    public function getContent(): IDataReader
    {
        return new XmlDataHandler($this->handler);
    }

}
