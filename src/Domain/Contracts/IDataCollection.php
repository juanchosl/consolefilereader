<?php

namespace HolaLuz\Domain\Contracts;

interface IDataCollection extends \Iterator, \JsonSerializable, \Countable
{

}
