<?php

namespace HolaLuz\Infrastructure\Handlers\Filehandlers;

use HolaLuz\Infrastructure\Handlers\Filehandlers\CsvFileHandler;
use HolaLuz\Infrastructure\Handlers\Filehandlers\XmlFileHandler;
use HolaLuz\Infrastructure\Handlers\Filehandlers\JSONFileHandler;

class HandlerFactory
{

    public static function getInstance(string $filepath): IFileReader
    {
        $extension = strtoupper(pathinfo($filepath, PATHINFO_EXTENSION));
        switch ($extension) {
            case 'CSV':
                $handler = new CsvFileHandler($filepath);
                break;

            case 'XML':
                $handler = new XmlFileHandler($filepath);
                break;

            case 'JSON':
                $handler = new JSONFileHandler($filepath);
                break;

            default:
                throw new \Exception("Extension '{$extension}' is not a valid value for {$filepath}", 416);
                break;
        }
        return $handler;
    }

}
