<?php

namespace HolaLuz\Infrastructure\Handlers\Datahandlers;

class XmlDataHandler implements IDataReader
{

    private $data;

    public function __construct(\SimpleXMLElement $data)
    {
        $this->data = $data;
    }

    public function getData(): array
    {
        $content = [];
        foreach ($this->data->reading as $reading) {
            $content[] = (object) [
                        'client' => (string) $reading['clientID'],
                        'period' => (string) $reading['period'],
                        'reading' => (string) $reading,
            ];
        }
        return $content;
    }

}
