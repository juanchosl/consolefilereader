<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace HolaLuz\Infrastructure\Repositories;

use HolaLuz\Infrastructure\Handlers\Datahandlers\IDataReader;
use HolaLuz\Domain\Contracts\IDataRepository;
use HolaLuz\Domain\Collections\UserDataCollection;
use HolaLuz\Domain\Entities\UserData;

class UserDataRepository implements IDataRepository
{

    private $data_reader;

    public function __construct(IDataReader $data_reader)
    {
        $this->data_reader = $data_reader;
    }

    public function getData(): UserDataCollection
    {
        $collection = new UserDataCollection();
        foreach ($this->data_reader->getData() as $data) {
            $collection->add(new UserData($data->client, $data->period, $data->reading));
        }
        return $collection;
    }

}
