<?php

namespace HolaLuz\Infrastructure\Handlers\Filehandlers;

use HolaLuz\Infrastructure\Handlers\Datahandlers\IDataReader;

interface IFileReader
{

    public function getContent(): IDataReader;
}
