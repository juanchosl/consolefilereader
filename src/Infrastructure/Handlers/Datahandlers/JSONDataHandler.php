<?php

namespace HolaLuz\Infrastructure\Handlers\Datahandlers;

class JSONDataHandler implements IDataReader
{

    private $data;

    public function __construct(string $data)
    {
        $this->data = json_decode($data, false);
    }

    public function getData(): array
    {
        $content = [];
        foreach ($this->data as $reading) {
            $content[] = (object) [
                        'client' => $reading->client,
                        'period' => $reading->period,
                        'reading' => $reading->reading,
            ];
        }
        return $content;
    }

}
