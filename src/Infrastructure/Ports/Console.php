<?php

namespace HolaLuz\Infrastructure\Ports;

use dekor\ArrayToTextTable;
use HolaLuz\Infrastructure\Handlers\Filehandlers\HandlerFactory;
use HolaLuz\Domain\Contracts\IDataCollection;
use HolaLuz\Infrastructure\Repositories\UserDataRepository;

class Console extends Application
{

    public function run()
    {
        $handler = new UserDataRepository(HandlerFactory::getInstance($this->args[1])->getContent());
        switch ($this->args[2] ?? null) {
            case 'json':
                return $this->responseAsJson(call_user_func($this->action, $handler));
                break;

            default:
                return $this->responseAsTable(call_user_func($this->action, $handler));
                break;
        }
    }

    public function responseAsTable(IDataCollection $result, $code = 0)
    {
        $result = (new ArrayToTextTable($result->toArray()))->render();
        $this->response($result, $code);
    }

    public function responseAsJson(IDataCollection $result, $code = 0)
    {
        $result = json_encode($result, JSON_PRETTY_PRINT);
        $this->response($result, $code);
    }

    public function response(string $result, int $code = 0)
    {
        print_r($result);
        echo PHP_EOL;
        exit($code);
    }

}
