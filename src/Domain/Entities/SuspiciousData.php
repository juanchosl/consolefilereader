<?php

namespace HolaLuz\Domain\Entities;

class SuspiciousData
{

    public function __construct(
            public $client,
            public $month,
            public $suspicious,
            public $median
    )
    {

    }

    public function getClient(): string
    {
        return $this->client;
    }

    public function getMonth(): string
    {
        return $this->month;
    }

    public function getSuspicious(): float
    {
        return $this->suspicious;
    }

    public function getMedian(): float
    {
        return $this->median;
    }

}
