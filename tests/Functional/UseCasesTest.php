<?php

namespace HolaLuz\Tests\Functional;

use PHPUnit\Framework\TestCase;
use HolaLuz\Application\GetSuspiciousUseCase;
use HolaLuz\Domain\Entities\SuspiciousData;
use HolaLuz\Domain\Collections\SuspiciousDataCollection;
use HolaLuz\Infrastructure\Repositories\UserDataRepository;
use HolaLuz\Infrastructure\Handlers\Filehandlers\HandlerFactory;

class UseCasesTest extends TestCase
{

    public function testSuspiciousByFileCsv()
    {
        $this->getSuspiciousByFile(__DIR__ . '/../../etc/2016-readings.csv');
    }

    public function testSuspiciousByFileXml()
    {
        $this->getSuspiciousByFile(__DIR__ . '/../../etc/2016-readings.xml');
    }

    public function testSuspiciousByFileJson()
    {
        $this->getSuspiciousByFile(__DIR__ . '/../../etc/2016-readings.json');
    }

    protected function getSuspiciousByFile($filepath)
    {
        $this->assertFileExists($filepath);
        $handler = new UserDataRepository(HandlerFactory::getInstance($filepath)->getContent());
//        $handler = HandlerFactory::getInstance($filepath)->getContent();
        $usecase = new GetSuspiciousUseCase;
        $suspicious = call_user_func($usecase, $handler);
        $this->assertInstanceOf(SuspiciousDataCollection::class, $suspicious);
        $this->assertContainsOnlyInstancesOf(SuspiciousData::class, $suspicious);
    }

}
