<?php

namespace HolaLuz\Domain;

use HolaLuz\Domain\Contracts\IDataCollection;
use HolaLuz\Domain\Entities\SuspiciousData;
use HolaLuz\Domain\Entities\UserData;
use HolaLuz\Domain\Collections\SuspiciousDataCollection;

class ReadingService
{

    const ALGORITHM_MEDIAN = 1;
    const ALGORITHM_AVERAGE = 2;

    private $suspicious_algo;
    private $percent_variation;
    private IDataCollection $data_content;
    private $data = [];

    /**
     * Constructor
     * @param IDataReader $data_reader Handler for data origin
     */
    public function __construct(int $suspicious_algorithm, int $percent_variation)
    {
        $this->setSuspisiousAlgorithm($suspicious_algorithm);
        $this->setPercentVariation($percent_variation);
    }

    public function setPercentVariation(int $percent_variation)
    {
        $this->percent_variation = $percent_variation;
    }

    public function setSuspisiousAlgorithm(int $suspicious_algorithm)
    {
        $this->suspicious_algo = $suspicious_algorithm;
    }

    protected function read(): void
    {
        foreach ($this->data_content as $value) {
            list($year, $month) = explode('-', $value->getPeriod());
            $this->data[$value->getUserId()][$year][$month] = $value->getValue();
        }
    }

    /**
     * Get suspicious values from a N UserData values
     * @param UserData $user_data N UserData parameters to check
     * @return array Suspicious values
     */
    public function getSuspiciousByData(IDataCollection $user_data): IDataCollection
    {
        $this->data_content = $user_data;
        $this->read();
        return $this->getSuspicious();
    }

    /**
     * Get suspicious values from the last Handler
     * @return array Suspicious values
     */
    protected function getSuspicious(): IDataCollection
    {
        $calculated = ($this->suspicious_algo == self::ALGORITHM_AVERAGE) ? $this->getAverage() : $this->getMedium();
        $response = new SuspiciousDataCollection();
        foreach ($this->data as $user_id => $data) {
            foreach ($data as $year => $values) {
                $medium = $calculated[$user_id][$year];
                $min = $this->getMinimum($medium);
                $max = $this->getMaximum($medium);
                foreach ($values as $month => $value) {
                    if ($value > $max OR $value < $min) {
                        $response->add(new SuspiciousData($user_id, $month, $value, $medium));
                    }
                }
            }
        }
        return $response;
    }

    protected function getMinimum($value)
    {
        return $value * (0 + ($this->percent_variation / 100));
    }

    protected function getMaximum($value)
    {
        return $value * (1 + ($this->percent_variation / 100));
    }

    protected function getMedium()
    {
        $calculated = [];
        foreach ($this->data as $user_id => $year_data) {
            foreach ($year_data as $year => $month_data) {
                foreach ($month_data as $month => $data) {
                    $calculated[$user_id]['values'][] = $data;
                }
                natsort($calculated[$user_id]['values']);
                $calculated[$user_id][$year] = ($calculated[$user_id]['values'][6] + $calculated[$user_id]['values'][7]) / 2;
            }
        }
        return $calculated;
    }

    protected function getAverage()
    {
        $response = [];
        foreach ($this->data as $user_id => $data) {
            foreach ($data as $year => $values) {
                $response[$user_id][$year] = array_sum($values) / 12;
            }
        }
        return $response;
    }

}
