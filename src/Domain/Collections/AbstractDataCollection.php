<?php

namespace HolaLuz\Domain\Collections;

use HolaLuz\Domain\Contracts\IDataCollection;

abstract class AbstractDataCollection implements IDataCollection
{

    protected $data = [];

    public function clear(): void
    {
        $this->data = [];
    }

    public function isEmpty(): bool
    {
        return empty($this->data);
    }

    public function toArray(): array
    {
        $this->rewind();
        return json_decode(json_encode($this->data), true);
    }

    public function current(): mixed
    {
        return current($this->data);
    }

    public function key(): mixed
    {
        return key($this->data);
    }

    public function next(): void
    {
        next($this->data);
    }

    public function rewind(): void
    {
        reset($this->data);
    }

    public function valid(): bool
    {
        return !is_null($this->key());
    }

    public function jsonSerialize(): mixed
    {
        return json_encode($this->data);
    }

    public function count(): int
    {
        return count($this->data);
    }

}
