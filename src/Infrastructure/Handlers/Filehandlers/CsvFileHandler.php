<?php

namespace HolaLuz\Infrastructure\Handlers\Filehandlers;

use HolaLuz\Infrastructure\Handlers\Datahandlers\IDataReader;
use HolaLuz\Infrastructure\Handlers\Datahandlers\ArrayDataHandler;

class CsvFileHandler implements IFileReader
{

    private $content = [];

    public function __construct($filepath)
    {
        $this->content = array_map('str_getcsv', file($filepath));
    }

    public function getContent(): IDataReader
    {
        return new ArrayDataHandler($this->content);
    }

}
