<?php

namespace HolaLuz\Infrastructure\Handlers\Datahandlers;

interface IDataReader
{

    public function getData(): array;
}
