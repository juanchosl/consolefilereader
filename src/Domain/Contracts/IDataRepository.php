<?php

namespace HolaLuz\Domain\Contracts;

use HolaLuz\Domain\Contracts\IDataCollection;

interface IDataRepository
{

    public function getData(): IDataCollection;
}
