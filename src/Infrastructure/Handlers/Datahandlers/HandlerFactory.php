<?php

namespace HolaLuz\Infrastructure\Handlers\Datahandlers;

use HolaLuz\Infrastructure\Handlers\Datahandlers\ArrayDataHandler;
use HolaLuz\Infrastructure\Handlers\Datahandlers\XmlDataHandler;
use HolaLuz\Infrastructure\Handlers\Datahandlers\JSONDataHandler;

class HandlerFactory
{

    public static function getInstance(mixed $data): IDataReader
    {
        if ($data instanceof \SimpleXMLElement) {
            return new XmlDataHandler($data);
        } elseif (is_array($data)) {
            return new ArrayDataHandler($data);
        } elseif (is_string($data) && !is_null(json_decode($data))) {
            return new JSONDataHandler($data);
        }
        throw new \Exception("Data does not contains a valid values", 416);
    }

}
