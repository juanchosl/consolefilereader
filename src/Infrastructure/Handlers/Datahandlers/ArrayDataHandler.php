<?php

namespace HolaLuz\Infrastructure\Handlers\Datahandlers;

class ArrayDataHandler implements IDataReader
{

    private $data = [];

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function getData(): array
    {
        $keys = $this->data[0];
        $data = array_slice($this->data, 1);
        $content = [];
        foreach ($data as $values) {
            $content[] = (object) array_combine($keys, $values);
        }
        return $content;
    }

}
