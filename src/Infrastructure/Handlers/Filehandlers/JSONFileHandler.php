<?php

namespace HolaLuz\Infrastructure\Handlers\Filehandlers;

use HolaLuz\Infrastructure\Handlers\Datahandlers\JSONDataHandler;
use HolaLuz\Infrastructure\Handlers\Datahandlers\IDataReader;

class JSONFileHandler implements IFileReader
{

    private $data;

    public function __construct($filepath)
    {
        $this->data = file_get_contents($filepath);
    }

    public function getContent(): IDataReader
    {
        return new JSONDataHandler($this->data);
    }

}
