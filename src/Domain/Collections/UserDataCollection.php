<?php

namespace HolaLuz\Domain\Collections;

use HolaLuz\Domain\Entities\UserData;

class UserDataCollection extends AbstractDataCollection
{

    public function add(UserData $user): self
    {
        array_push($this->data, $user);
        return $this;
    }

    public function copy(): UserDataCollection
    {
        $col = new UserDataCollection();
        foreach ($this->data as $data) {
            $col->add($data);
        }
        return $col;
    }

}
