<?php

namespace HolaLuz\Application;

use HolaLuz\Domain\ReadingService;
use HolaLuz\Domain\Contracts\IDataRepository;
use HolaLuz\Domain\Contracts\IDataCollection;

class GetSuspiciousUseCase
{

    public function __invoke(IDataRepository $handler): IDataCollection
    {
        $service = new ReadingService(ReadingService::ALGORITHM_MEDIAN, 50);
        return $service->getSuspiciousByData($handler->getData());
    }

}
