<?php

namespace HolaLuz\Infrastructure\Ports;

use HolaLuz\Application\GetSuspiciousUseCase;

abstract class Application
{

    protected $action;

    abstract public function response(string $response, int $code);

    public function __construct(protected array $args)
    {
        set_exception_handler([$this, 'MyExceptionHandler']);
        set_error_handler([$this, 'MyErrorHandler']);
        $this->action = new GetSuspiciousUseCase();
    }

    public function MyExceptionHandler($exception)
    {
        return $this->response($exception->getMessage(), $exception->getCode());
    }

    public function MyErrorHandler($errNo, $errMsg, $file, $line)
    {
        return $this->response("[{$file}:{$line}]" . PHP_EOL . $errMsg, $errNo);
    }

}
