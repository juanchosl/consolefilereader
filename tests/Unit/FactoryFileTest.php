<?php

namespace HolaLuz\Tests\Unit;

use PHPUnit\Framework\TestCase;
use HolaLuz\Infrastructure\Handlers\Filehandlers\HandlerFactory;
use HolaLuz\Infrastructure\Handlers\Filehandlers\CsvFileHandler;
use HolaLuz\Infrastructure\Handlers\Filehandlers\XmlFileHandler;
use HolaLuz\Infrastructure\Handlers\Filehandlers\JSONFileHandler;

class FactoryFileTest extends TestCase
{

    public function testCsvHandler()
    {
        $filepath = __DIR__ . '/../../etc/2016-readings.csv';
        $handler = HandlerFactory::getInstance($filepath);
        $this->assertInstanceOf(CsvFileHandler::class, $handler);
    }

    public function testJsonHandler()
    {
        $filepath = __DIR__ . '/../../etc/2016-readings.json';
        $handler = HandlerFactory::getInstance($filepath);
        $this->assertInstanceOf(JSONFileHandler::class, $handler);
    }

    public function testXmlHandler()
    {

        $filepath = __DIR__ . '/../../etc/2016-readings.xml';
        $handler = HandlerFactory::getInstance($filepath);
        $this->assertInstanceOf(XmlFileHandler::class, $handler);
    }

    public function testErrorHandler()
    {
        $this->expectException(\Exception::class);
        $filepath = __DIR__ . '/../../etc/2016-readings.sql';
        HandlerFactory::getInstance($filepath);
//        try {
//            $this->assertFalse(true);
//        } catch (\Exception $ex) {
//            $this->assertTrue(true);
//        }
    }

}
