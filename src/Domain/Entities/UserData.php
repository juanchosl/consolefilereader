<?php

namespace HolaLuz\Domain\Entities;

class UserData
{

    public function __construct(
            private $user_id,
            private $period,
            private $value
    )
    {

    }

    public function getUserId(): string
    {
        return $this->user_id;
    }

    public function getPeriod(): string
    {
        return $this->period;
    }

    public function getValue(): float
    {
        return $this->value;
    }

}
